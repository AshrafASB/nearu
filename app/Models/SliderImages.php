<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class SliderImages extends Model
{

    protected $guarded = [];
    protected $table = 'slider_images';


    public const URL = 'slider_images';
    public const DIR_IMAGE_UPLOADS = 'slider_images';


//    Types
    public const URL_EXTERNAL = 1;
    public const MERCHANT = 2;
//    public const ITEM = 3;



    protected static function boot()
    {
        parent::boot(); // Change the autogenerated stub
        static::addGlobalScope('orderedBy', function (Builder $builder) {
            $builder->orderBy('ordered');
        });
    }

    public function getImageAttribute($value)
    {
        return is_null($value) ? defaultImage() : asset($value);
    }
    public function getTypeNameAttribute()
    {
        switch ($this->type) {
            case self::URL_EXTERNAL:
                return api('URL_EXTERNAL');
            case self::MERCHANT:
                return api('MERCHANT');
            default:
                return api('unknown status');
                break;
        }
    }


    public function merchant()
    {
        return $this->belongsTo(Merchant::class,'merchant_id','id');
    }

    public function getActionButtonsAttribute()
    {
        $button = '';
        $button .= '<a href="' . route('manager.slider.edit', $this->id)
            . '" class="btn btn-icon btn-danger "><i class="la la-pencil"></i></a> ';
        $button .= '<button type="button" data-id="' . $this->id
            . '" data-toggle="modal" data-target="#deleteModel" class="deleteRecord btn btn-icon btn-danger"><i class="la la-trash"></i></button>';
        return $button;
    }

}
