<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class DeliveryStatusTimeLine extends Model
{    use HasTranslations;
    use SoftDeletes;
    public $translatable = ['key_name', 'details'];


    protected $table = 'delivery_time_line_status';
    protected $guarded = [];

//    Relations
    public function delivery()
    {
        return $this->belongsTo(Delivery::class);
    }
}
