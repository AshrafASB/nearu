<?php

namespace App\Http\Controllers\Api\v1;

use App\Events\NewOrderEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\v1\CouponResource;
use App\Http\Resources\Api\v1\ItemReOrderResource;
use App\Http\Resources\Api\v1\OrderResource;
use App\Models\Address;
use App\Models\Branch;
use App\Models\ClientOrderRate;
use App\Models\Coupon;
use App\Models\Item;
use App\Models\ItemAddon;
use App\Models\ItemPrice;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderItemAddon;
use App\Models\OrderStatusTimeLine;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class OrderController extends Controller
{
    private $tax = 0;
    private $commission = 0;
    private $commission_delivery = 0;
    private $kilo_cost = 0;
    private $commission_tap_payment_gateway = 0;

    public function __construct()
    {
        $tax = getSettings('tax')->value;
        $commission_tap_payment_gateway = setting('commission_tap_payment_gateway');
        $commission = getSettings('commission')->value;
        $kilo_cost = getSettings('kilo_cost')->value;
        $commission_delivery = getSettings('commission_delivery')->value;

        $this->tax = ($tax) ? $tax : (double)0;
        $this->commission = ($commission) ? (double)$commission : (double)0.05;
        $this->kilo_cost = ($kilo_cost) ? $kilo_cost : (double)0;
        $this->commission_tap_payment_gateway = ($commission_tap_payment_gateway) ? $commission_tap_payment_gateway : (double)0.05;

        //        there is no commission when the paid type is payment when receiving
//        if (isset(request()->paid_type) && request()->paid_type == Order::CASH) $this->commission_delivery = 0;else
        $this->commission_delivery = ($commission_delivery) ? $commission_delivery : (double)0.10;
    }

    public function currentTrackMap(Request $request, $id)
    {
        $request['except_arr_resource'] = ['rate', 'status_time_line', 'coupon', 'meals'];
        $order = Order::query()->where('user_id', apiUser()->id)->withoutGlobalScope('paidOrder')->findOrFail($id);
        if ($order->status != Order::ON_WAY) return apiError(api("can not see this order on map"));
        return apiSuccess(new OrderResource($order));
    }

    public function getAllHours(Request $request, $id)
    {
        $request->validate([
            'pick_up_time' => 'required|date_format:Y-m-d',
        ]);
        $branch = Branch::query()->notDraft()->find($id);
        if (!$branch) return apiError(api('Branch Not Found'));
        $newArr = [];
        foreach (getDayHours() as $index => $hr) {
            if (!$branch->isOpenInThisHr($hr, Carbon::parse($request->pick_up_time)->dayOfWeek) || $branch->ordersCount($hr) >= $branch->max_orders) {
                $newArr[$hr]['is_free'] = false;
            } else {
                $newArr[$hr]['is_free'] = true;
            }
        }
        return apiSuccess($newArr);
    }


    private function checkCouponUsage($request)
    {
        $key = $request->get('coupon');
        $coupon = Coupon::query()->whereHas('branches', function ($query) use ($request) {
            $query->where('branches.id', $request->branch_id);
        })->whereDate('expire_at', '>=', now())->where('code', $key)->first();
        if (!$coupon) return null;

        //Check Coupon Usage
        $order_coupon = Order::query()->where('coupon_id', $coupon->id)->count();
        if ($coupon->number_usage <= $order_coupon) return null;

        //Check Coupon User Usage
        if (Auth::guard('api')->check()) {
            $user_orders = Order::query()->where('coupon_id', $coupon->id)->where('user_id', apiUser()->id)->count();
            if (!is_null($coupon->number_users) && ($coupon->number_users <= $user_orders)) return null;
        }


        return $coupon;
    }

    public function checkCoupon(Request $request)
    {
        $request->validate([
            'coupon' => 'required',
            'branch_id' => 'required|exists:branches,id'
        ]);

        $coupon = $this->checkCouponUsage($request);
        return (isset($coupon)) ? apiSuccess(new CouponResource($coupon), api('Coupon Is Valid')) : apiError(api('Invalid Coupon'), 422);
    }


    public function checkOrder(Request $request)
    {
        $request->validate([
            'branch_id' => 'required|exists:branches,id',
            'meals.*.id' => 'required|numeric',
            'meals.*.quantity' => 'required|numeric',
            'meals.*.price' => 'required|numeric',
            'meals.addon' => 'nullable|numeric',
            'coupon' => 'nullable|string',
        ]);
        $branch = Branch::query()->find($request->get('branch_id'));
        if (!$branch) return apiError(api('Branch Not Found'));

        $result = $this->getOrderDetails($branch);

        if (!is_array($result)) return apiError($result);

        $res = [
            'total_cost' => $result['total_cost'],
            'meals_cost' => $result['meals_cost'],
            'tax_cost' => $result['tax_cost'],
            'delivery_cost' => $result['delivery_cost'],
            'coupon_discount' => $result['coupon_discount'],
            'wallet_balance' => $result['wallet_balance'],
        ];
        return apiSuccess($res);
    }

    public function checkout(Request $request)
    {
        $request['except_arr_resource'] = ['items'];
        $request->validate([
            'address_id' => 'required|exists:addresses,id',
            'branch_id' => 'required|exists:branches,id',
            'order_type' => 'required|in:' . Order::DIRECT . ',' . Order::SCHEDULE,
            'paid_type' => 'required|in:' . Order::VISA . ',' . Order::WALLET . ',' . Order::CASH,
            'pick_up_time' => 'required_if:order_type,' . Order::SCHEDULE . '|date_format:Y-m-d H:i:s',
            'meals.*.id' => 'required|numeric',
            'meals.*.quantity' => 'required|numeric',
            'meals.*.price' => 'required|numeric',
            'meals.addon' => 'nullable|numeric',
            'coupon' => 'nullable|string',
            'note' => 'nullable|string',
            //'payment_id' => 'required_if:paid_type,visa',
        ]);
        $branch = Branch::query()->find($request->get('branch_id'));
        if (!$branch) return apiError(api('Branch Not Found'));

//        dd($branch->ordersCount(), $branch->max_orders,Carbon::now()->addHour()->format(DATE_FORMAT_FULL));
        if (!$branch->isOpenInThisHr(Carbon::now()->hour, Carbon::now()->dayOfWeek)) return apiError(api('Branch Is Closed Now Please Try Again'));
        if ($branch->ordersCount() >= $branch->max_orders) return apiError(api('Branch Is Busy  Please Try Again !'));
        $result = $this->getOrderDetails($branch);
        if (!is_array($result)) return apiError($result);
        $user = apiUser();
        $total_cost = $result['total_cost'];
        $meals_cost = $result['meals_cost'];
        $delivery_cost = $result['delivery_cost'];
        $commission_delivery_cost = $result['commission_delivery_cost'];
        $driver_slice = $result['driver_slice'];
        $tax_cost = (float)number_format($result['tax_cost'], 2, '.', '');
        $commission_cost = (float)number_format($result['commission_cost'], 2, '.', '');
        $coupon_discount = $result['coupon_discount'];
        $coupon_id = $result['coupon_id'];

        $manager_commission_cost = $result['manager_commission_cost'];
        $wallet_balance = $result['wallet_balance'];
        $tap_payment_gateway_cost = $result['tap_payment_gateway_cost'];
        $branch_slice = $result['branch_slice'];

        if ($request->get('paid_type') == Order::WALLET) {
            if ($user->user_wallet < $total_cost) {
                return apiError(api('Wallet Not Enough To Complete This Order'), 422);
            }
        }


        //Create Order
        $order = DB::transaction(function () use ($request, $branch, $total_cost, $manager_commission_cost, $tap_payment_gateway_cost, $branch_slice, $meals_cost, $delivery_cost, $commission_delivery_cost, $driver_slice, $tax_cost, $commission_cost, $coupon_discount, $coupon_id) {
            //Create Order
            $address = Address::findOrFail($request->address_id);
            $order = Order::query()->create([
                'user_id' => apiUser()->id,
                'lat' => $address->lat,
                'lng' => $address->lng,
                'address_id' => $address->id,
                'branch_id' => $branch->id,
                'status' => Order::PENDING,
                'type' => $request->order_type,
                'total_cost' => $total_cost,
                'meals_cost' => $meals_cost,
                'delivery_cost' => $delivery_cost,
                'commission_delivery_cost' => $commission_delivery_cost,
                'driver_slice' => $driver_slice,
                'tax_cost' => $tax_cost,
                'commission_cost' => $commission_cost,
                'coupon_discount' => $coupon_discount,
                'manager_commission_cost' => $manager_commission_cost,
                'tap_payment_gateway_cost' => $tap_payment_gateway_cost,
                'branch_slice' => $branch_slice,
                'coupon_id' => $coupon_id,
                'paid' => $request->paid_type == Order::WALLET ? 1 : 0,
                'paid_type' => $request->paid_type,
                'pick_up_time' => ($request->order_type == Order::SCHEDULE) ? $request->pick_up_time : Carbon::now()->addHour(),
                'note' => $request->get('note', null),
            ]);
            $statusTimeLine = OrderStatusTimeLine::create([
                'order_id' => $order->id,
                'key' => Order::PENDING,
                'key_name' => [
                    'ar' => 'تم استلام الطلب',
                    'en' => 'Order received successfully',
                ],
                'value' => Carbon::now(),
                'details' => [
                    'ar' => 'بانتظار قبول المتجر للطلب',
                    'en' => 'Wait merchant to accept your order',
                ],
            ]);
            if ($request->get('paid_type') == Order::WALLET) {
//                TODO check this wallet
                Wallet::query()->create([
                    'user_id' => apiUser()->id,
                    't_type' => Wallet::NEW_ORDER,
                    'order_id' => $order->id,
                    'amount' => $total_cost,
                ]);
            }
            //Create Price & Addon
            foreach ($request->get('meals', []) as $key => $item) {
                $meal_quantity = isset($item['quantity']) ? $item['quantity'] : 1;
                $meal = Item::query()->currentBranch($branch->id)->find($item['id']);
                if ($meal) {
                    $meal_price = ItemPrice::query()->where('item_id', $meal->id)->find($item['price']);
                    if ($meal_price) {
                        $order_item = OrderItem::create([
                            'order_id' => $order->id,
                            'item_id' => $meal->id,
                            'quantity' => $meal_quantity,
                            'item_price_id' => $meal_price->id,
                            'price' => $meal_price->price,
                            'amount' => $meal_price->price * $meal_quantity,
                        ]);
                        if (isset($item['addon']) && $item['addon'] != '') {
                            $addons = explode(',', $item['addon']);
                            foreach ($addons as $addon) {
                                $meal_addon = ItemAddon::query()->where('item_id', $meal->id)->find($addon);
                                if ($meal_addon) {
                                    OrderItemAddon::query()->create([
                                        'order_item_id' => $order_item->id,
                                        'item_addon_id' => $meal_addon->id,
                                        'quantity' => $meal_quantity,
                                        'price' => $meal_addon->price,
                                        'amount' => $meal_addon->price * $meal_quantity,
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
            return $order;
        });
        $order->update([
            'uuid' => date('Y') . date('m') . $order->id,
        ]);
//TODO Send notification to provider new  order
//            event(new NewOrderEvent($order));

        return apiSuccess(new OrderResource($order), api('Order Created Successfully'));
    }

    public function orders(Request $request, $key)
    {
        $request['except_arr_resource'] = ['items'];

        $user = apiUser();
        $search = $request->get('search', false);
        $orders = Order::query()->latest('updated_at')->where('user_id', $user->id)->withoutGlobalScope('paidOrder');

        switch ($key) {
            case 'current':
                $orders = $orders->notCanceledOrder()->notCompletedOrder()->when($search, function ($query) use ($search) {
                    $query->where('uuid', 'like', '%' . $search . '%');
                })->paginate($this->perPage);
                return apiSuccess([
                    'items' => OrderResource::collection($orders->items()),
                    'paginate' => paginate($orders),
                ]);
            case 'completed':
                $orders = $orders->completedOrCanceledOrder()->when($search, function ($query) use ($search) {
                    $query->where('uuid', 'like', '%' . $search . '%');
                })->paginate($this->perPage);
                return apiSuccess([
                    'items' => OrderResource::collection($orders->items()),
                    'paginate' => paginate($orders),
                ]);
                break;
            default:
                $orders = $orders->completedOrder()->when($search, function ($query) use ($search) {
                    $query->where('uuid', 'like', '%' . $search . '%');
                })->paginate($this->perPage);
                return apiSuccess([
                    'items' => OrderResource::collection($orders->items()),
                    'paginate' => paginate($orders),
                ]);
        }
    }

    public function order(Request $request, $id)
    {
        $request['except_arr_resource'] = ['items'];
        $user = apiUser();
        $order = Order::query()->withoutGlobalScope('paidOrder')->where('user_id', $user->id)->findOrFail($id);
        return apiSuccess(new OrderResource($order));
    }


    public function rateOrder(Request $request, $id)
    {

        $request['except_arr_resource'] = ['items'];

        $request->validate([
            'rate' => 'required|in:1,2,3,4,5',
            'comment' => Rule::requiredIf(function () use ($request) {
                return in_array($request->get('rate'), [1, 2, 3]);
            }),
        ]);
        $user = apiUser();
        $order = Order::findOrFail($id);
        $branch = $order->branch;
        if ($order->status != Order::COMPLETED) return apiError(api('The status of the request does not allow it to be evaluated'), 422);
//        dd($order->ClientOrderRated);
        if ($order->ClientOrderRated) return apiError(api('Order Rated Previously'), 422);
        $store = new ClientOrderRate();
        $store->user_id = apiUser()->id;
        $store->order_id = $order->id;
        $store->branch_id = $branch->id;
        $store->stars_number = $request->get('rate');
        $store->content_rating = $request->get('comment');
        $store->save();
        $rates = ClientOrderRate::query()->whereHas('order', function ($query) use ($order) {
            $query->where('order_id', $order->id);
        })->avg('stars_number');
        $branch->update([
            'rate' => (float)$rates,
        ]);

//        TODO send notification to merchant rating
//        Notification::send($order->provider, new RateOrderNotification($order));
        return apiSuccess(new OrderResource($order), api('User Rated Successfully'));

    }


    public function reOrder(Request $request, $id)
    {
        $request['except_arr_resource'] = ['items'];

        $user = apiUser();
        $order = Order::query()->where('user_id', $user->id)->findOrFail($id);
        if ($order->status == Order::COMPLETED || $order->status == Order::CANCELED) {
            $orderItems = OrderItem::query()->where('order_id', $id)/*->whereIn('item_id', $items->pluck('id'))*/ ->get();
            return apiSuccess(ItemReOrderResource::collection($orderItems));
        } else {
            return apiError(api('Can not Reorder Before Complete Order'), 422);
        }
    }

    public function confirmOrder(Request $request, $id)
    {
        $request->validate([
            'payment_id' => 'required|string',
        ]);
        Log::emergency($request->all());
        Log::emergency($id);

        $user = Auth::user();
        $order = Order::query()->where('user_id', $user->id)
            ->where('status', '1')
            ->where('paid', 0)
            ->withoutGlobalScope('paidOrder')->findOrFail($id);

        $payment_id = $request->get('payment_id');
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.tap.company/v2/charges/$payment_id",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer sk_live_QkVtLbmW6H1oMRzXaYFecZIA"
            ),
        ));
        //test key : sk_test_C5S4VYNZxrUEt6uFoyjO8Rew
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return apiError(api('Payment Error'));
        } else {
            $res = json_decode($response);
            Log::notice('Pre Payment Check Response');
            if (isset($res->response) && optional($res->response)->code == "000") {
                Log::notice('Pre Payment Done');
//                $order->update([
//                    'paid' => 1,
//                    'payment_id' => $request->get('payment_id'),
//                ]);
                $order->paid = 1;
                $order->payment_id = $request->get('payment_id');
                $order->save();
                event(new NewOrderEvent($order));
                Log::notice('Payment Done');
            } else {
                return apiError(api('Payment Error'));
            }

        }
        return $this->sendResponse(new OrderResource($order), api('Order Successfully Sent To Branch'));
    }


    public function cancelOrder(Request $request, $id)
    {
        $request['except_arr_resource'] = ['items'];
        $user = apiUser();
        $order = Order::query()->where('user_id', $user->id)->findOrFail($id);
        if ($order->status != Order::PENDING /*|| !$order->paid*/) return apiError(api('Order Can Not be Canceled'), 422);
        $order->update([
            'status' => Order::CANCELED,
            'user_cancel' => Order::CLIENT_CANCELED,// CLIENT CANCELED THAT ORDER
            'status_time_line' => getNewEncodedArray(getAnonymousStatusObj(Order::CANCELED, 'CANCELED', null, api('Order Canceled')), $order->status_time_line),
        ]);
//        TODO SEND NOTIFICATION TO PROVIDER
//        event(new UserCancelOrderEvent($order));
        return apiSuccess(new OrderResource($order), api('Order Canceled Successfully'));
    }


    private function getOrderDetails($branch)
    {

        $request = request();
        if (apiUser()) {
            $request['lat'] = apiUser()->lat;
            $request['lng'] = apiUser()->lng;
        }
        $tax = $this->tax;
        $kilo_cost = $this->kilo_cost;
        $commission = $this->commission;
        $commission_delivery = $this->commission_delivery;
        $commission_tap_payment_gateway = $this->commission_tap_payment_gateway;
        $manager_commission_cost = 0;

        $total_cost = 0;
        $meals_cost = 0;
        $tax_cost = 0;
        $commission_cost = 0;
        $coupon_discount = 0;
        $distance = 0;
        if (apiUser()) {
            $package = apiUser()->packages()->where('expired', false)->first();
            $distance = distance($branch->lat, $branch->lng, apiUser()->lat, apiUser()->lng, 'K');
            $distance = (double)number_format($distance, 2);
            $delivery_cost = (double)number_format($distance * $kilo_cost, 2);
            if (isset($package) && $package->km_limit >= $distance) {
                $commission_delivery_cost = 0;// (double)number_format(($commission_delivery > 0) ? ($commission_delivery * $delivery_cost) : $delivery_cost, 2);
                $driver_slice = 0;//(double)number_format($delivery_cost - $commission_delivery_cost, 2);
            } else {
                $commission_delivery_cost = (double)number_format(($commission_delivery > 0) ? ($commission_delivery * $delivery_cost) : $delivery_cost, 2);
                $driver_slice = (double)number_format($delivery_cost - $commission_delivery_cost, 2);
            }
        } else {
            $delivery_cost = 0;
            $commission_delivery_cost = 0;
            $driver_slice = 0;
            $delivery_cost = 0;
        }

        foreach ($request->get('meals', []) as $key => $item) {
            $meal_quantity = isset($item['quantity']) ? $item['quantity'] : 1;
            $meal = Item::query()->currentBranch($branch->id)->find((int)$item['id']);
            if ($meal) {
                $meal_price = ItemPrice::query()->where('item_id', $meal->id)->notDraft()->find($item['price']);
                if ($meal_price) {
                    $price = $meal_price->price;
                    $meals_cost += $price * $meal_quantity;
                    if (isset($item['addon']) && $item['addon'] != '') {
                        $addons = explode(',', $item['addon']);
                        foreach ($addons as $addon) {
                            $meal_addon = ItemAddon::query()->where('item_id', $meal->id)->notDraft()->find($addon);
                            if ($meal_addon) {
                                $meals_cost += $meal_addon->price * $meal_quantity;
                            } else {
                                return api('Meal Item addons with id = ' . $addon . ' does not exist');
                            }
                        }
                    } else {
//                        return api('check addons format??');
                    }
                } else {
                    return api('Meal Item price with id = ' . $item['price'] . ' does not exist');
                }
            } else {
                return api('Meal with id = ' . $item['id'] . ' does not exist');
            }
        }
        if ($request->get('coupon', false)) {
            $key = $request->get('coupon', false);
            $coupon = Coupon::query()->whereDate('expire_at', '>=', now())->where('code', $key)->first();
            if ($coupon) {
                //Check Coupon Usage
                $order_coupon = Order::query()->where('coupon_id', $coupon->id)->count();
//                    first check the total usage number
                if ($coupon->number_usage > $order_coupon) {
//                    check if logged in user
                    if (Auth::guard('api')->check()) {
                        //Check Coupon User Usage
                        $user_orders = Order::query()->where('coupon_id', $coupon->id)->where('user_id', apiUser()->id)->count();
//                            check usage foreach user
                        if (!is_null($coupon->number_users) && ($coupon->number_users > $user_orders)) {
                            if ($coupon->type == Coupon::RATIO) {
                                $coupon_discount = $coupon->amount > 0 ? $meals_cost * ($coupon->amount / 100) : 0;
                                if ($meals_cost <= $coupon_discount) {
                                    $coupon_discount = $meals_cost;//لو كان سعر الكوبون اكبر من سعر السلة ف بدي بس اخصم سعر السلة واخلي سعر الكوبون الي تم خصمه بسعر السلة فقط
//                                    $meals_cost = 0;
                                }
                            } else {
                                if ($meals_cost > $coupon->amount) {
                                    $coupon_discount = $coupon->amount;
                                } elseif ($meals_cost <= $coupon->amount) {
                                    $coupon_discount = $meals_cost;//لو كان سعر الكوبون اكبر من سعر السلة ف بدي بس اخصم سعر السلة واخلي سعر الكوبون الي تم خصمه بسعر السلة فقط
//                                    $meals_cost = 0;
                                }
                            }
                        } else {
                            return api('You have exceeded the allowed number to use this coupon');
                        }
                    } else {
                        if ($coupon->type == Coupon::RATIO) {
                            $coupon_discount = $coupon->amount > 0 ? $meals_cost * ($coupon->amount / 100) : 0;
                            if ($meals_cost <= $coupon_discount) {
                                $coupon_discount = $meals_cost;//لو كان سعر الكوبون اكبر من سعر السلة ف بدي بس اخصم سعر السلة واخلي سعر الكوبون الي تم خصمه بسعر السلة فقط
//                                $meals_cost = 0;
                            }
                        } else {
                            if ($meals_cost > $coupon->amount) {
                                $coupon_discount = $coupon->amount;
                            } elseif ($meals_cost <= $coupon->amount) {
                                $coupon_discount = $meals_cost;//لو كان سعر الكوبون اكبر من سعر السلة ف بدي بس اخصم سعر السلة واخلي سعر الكوبون الي تم خصمه بسعر السلة فقط
//                                $meals_cost = 0;
                            }
                        }
                    }
                } else {
                    return api('This coupon cannot be used due to more than the total number of times used');
                }
            } else {
                return api('Wrong Coupon code');
            }
        }


        $tax_cost = ($meals_cost * $tax);
        $commission_cost = ($meals_cost * $commission);
        $branch_slice = $meals_cost - $commission_cost;
        $tap_payment_gateway_cost = $total_cost * $commission_tap_payment_gateway;
        $manager_commission_cost = $commission_cost + $commission_delivery_cost - $tap_payment_gateway_cost;
        $total_cost = $meals_cost + $delivery_cost + $tax_cost;// + $commission_cost - $coupon_discount;

        $res = [
            'total_cost' => (float)number_format(($total_cost - $coupon_discount), 2),
            'manager_commission_cost' => (float)number_format($manager_commission_cost, 2),
            'meals_cost' => (float)number_format($meals_cost, 2),
            'tax_cost' => (float)number_format($tax_cost, 2),
            'delivery_cost' => (float)number_format($delivery_cost, 2),
            'commission_delivery_cost' => (float)number_format($commission_delivery_cost, 2),
            'driver_slice' => (float)number_format($driver_slice, 2),
            'tap_payment_gateway_cost' => (float)number_format($tap_payment_gateway_cost, 2),
            'branch_slice' => (float)number_format($branch_slice, 2),
            'commission_cost' => (float)number_format($commission_cost, 2),
            'coupon_discount' => (float)number_format($coupon_discount, 2),
            'coupon_id' => isset($coupon) ? $coupon->id : null,
            'wallet_balance' => Auth::guard('api')->check() ? Auth::guard('api')->user()->user_wallet : 0,
            'distance' => $distance,
        ];
        return $res;

    }
}
