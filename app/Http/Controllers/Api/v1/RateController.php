<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\v1\BranchResource;
use App\Http\Resources\Api\v1\OrderResource;
use App\Http\Resources\Api\v1\RateResource;
use App\Http\Resources\Api\v1\User\ProfileResource;
use App\Models\Branch;
use App\Models\ClientDriverRate;
use App\Models\ClientOrderRate;
use App\Models\DriverClientRate;
use App\Models\Order;
use App\Models\User;
use App\Notifications\GeneralWithTitleAsArrayNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\Rule;

class RateController extends Controller
{

    public function get_driver_ratings(Request $request, $id)
    {
        $user = User::driver()->findOrFail($id);
        $ratings = ClientDriverRate::query()->whereHas('order', function ($query) use ($user) {
            $query->withoutGlobalScope('paidOrder')->where('driver_id', $user->id);
        })->latest()->get();
        return apiSuccess([
            'user' => new ProfileResource($user),
            'ratings' => RateResource::collection($ratings),
        ]);
    }

    public function get_user_ratings(Request $request, $id)
    {
        $user = User::client()->findOrFail($id);
        $ratings = DriverClientRate::query()->whereHas('order', function ($query) use ($user) {
            $query->withoutGlobalScope('paidOrder')->where('user_id', $user->id);
        })->latest()->get();
        return apiSuccess([
            'user' => new ProfileResource($user),
            'ratings' => RateResource::collection($ratings),
        ]);
    }

    public function get_branch_ratings(Request $request, $id)
    {
        $branch = Branch::findOrFail($id);
        $ratings = ClientOrderRate::query()->whereHas('order', function ($query) use ($branch) {
            $query->withoutGlobalScope('paidOrder')->where('branch_id', $branch->id);
        })->latest()->get();
        return apiSuccess([
            'branch' => new BranchResource($branch),
            'ratings' => RateResource::collection($ratings),
        ]);
    }

    public function client_rate_branch(Request $request, $id)
    {
        $request['except_arr_resource'] = ['items'];
        $request->validate([
            'rate' => 'required|in:1,2,3,4,5',
            'comment' => Rule::requiredIf(function () use ($request) {
                return in_array($request->get('rate'), [1, 2, 3]);
            }),
        ]);
        $user = apiUser();
        $order = Order::query()
            ->withoutGlobalScope('paidOrder')
            ->where('user_id', $user->id)->findOrFail($id);
        if ($order->clientRateBranch) return apiError(api('Order Rated Previously'), 422);
        if ($order->status != Order::COMPLETED) return apiError(api('The status of the request does not allow it to be evaluated'), 422);

        ClientOrderRate::query()->create([
            'order_id' => $id,
            'branch_id' => $order->branch_id,
            'user_id' => apiUser()->id,
            'stars_number' => $request->get('rate'),
            'content_rating' => $request->get('comment'),
        ]);
        $rates = ClientOrderRate::query()
            ->whereHas('order', function ($query) use ($order) {
                $query->where('order_id', $order->id);
            })->avg('stars_number');
        $order->branch->update([
            'rate' => (float)$rates,
        ]);
        $branch = $order->branch;
        $merchant = isset($branch) ? $branch->merchant : null;

        $title = [
            'ar' => notification_trans('Client rated you on order', ['uuid', $order->uuid], 'ar'),
            'en' => notification_trans('Client rated you on order', ['uuid', $order->uuid], 'en'),
        ];
        if (isset($branch)) Notification::send($branch, new GeneralWithTitleAsArrayNotification(null, $title, $title));
        if (isset($merchant)) Notification::send($merchant, new GeneralWithTitleAsArrayNotification(null, $title, $title));


        return apiSuccess(new OrderResource($order), api('Order Rated Successfully'));
    }

    public function client_rate_driver(Request $request, $id)
    {
        $request['except_arr_resource'] = ['items'];

        $request->validate([
            'rate' => 'required|in:1,2,3,4,5',
            'comment' => Rule::requiredIf(function () use ($request) {
                return in_array($request->get('rate'), [1, 2, 3]);
            }),
        ]);
        $user = apiUser();
        $order = Order::with(['delivery_completed'])/*->where('user_id', $user->id)->dd()*/ ->findOrFail($id);
        if ($order->status != Order::COMPLETED) return apiError(api('The status of the request does not allow it to be evaluated'), 422);
        if ($order->driverClientRated) return apiError(api('Order Rated Previously'), 422);
        $store = new ClientDriverRate();
        $delivery_completed = $order->delivery_completed;
        if (isset($delivery_completed)) {
            $store->user_id = apiUser()->id;
            $store->delivery_id = $delivery_completed->id;
            $store->driver_id = $delivery_completed->driver_id;
            $store->order_id = $order->id;
            $store->stars_number = $request->get('rate');
            $store->content_rating = $request->get('comment');
            $store->save();
            $rates = ClientDriverRate::query()->whereHas('order', function ($query) use ($order) {
                $query->where('order_id', $order->id);
            })->avg('stars_number');
            $delivery_completed->driver->update([
                'rate' => (float)$rates,
            ]);
            $title = [
                'ar' => notification_trans('Client rated you on order', ['uuid', $order->uuid], 'ar'),
                'en' => notification_trans('Client rated you on order', ['uuid', $order->uuid], 'en'),
            ];
            Notification::send($delivery_completed->driver, new GeneralWithTitleAsArrayNotification(null, $title, $title));
        }


        return apiSuccess(new OrderResource($order), api('User Rated Successfully'));
    }

    public function driver_rate_client(Request $request, $id)
    {
        $request['except_arr_resource'] = ['items'];

        $request->validate([
            'rate' => 'required|in:1,2,3,4,5',
            'comment' => Rule::requiredIf(function () use ($request) {
                return in_array($request->get('rate'), [1, 2, 3]);
            }),
        ]);
        $user = apiUser();
        $order = Order::with(['delivery_completed'])/*->where('user_id', $user->id)->dd()*/ ->findOrFail($id);
        if ($order->status != Order::COMPLETED) return apiError(api('The status of the request does not allow it to be evaluated'), 422);
//        dd($order,$order->driverRateClient);
        if ($order->driverRateClient) return apiError(api('Order Rated Previously'), 422);
        $store = new DriverClientRate();
        $delivery_completed = $order->delivery_completed;
        if (isset($delivery_completed)) {
            $store->driver_id = apiUser()->id;
            $store->user_id = $order->user_id;
            $store->delivery_id = $delivery_completed->id;
            $store->order_id = $order->id;
            $store->stars_number = $request->get('rate');
            $store->content_rating = $request->get('comment');
            $store->save();
            $rates = DriverClientRate::query()->whereHas('order', function ($query) use ($order) {
                $query->where('order_id', $order->id);
            })->avg('stars_number');
            $order->user->update([
                'rate' => (float)$rates,
            ]);
        }
        $title = [
            'ar' => notification_trans('Driver rated you on order', ['uuid', $order->uuid], 'ar'),
            'en' => notification_trans('Driver rated you on order', ['uuid', $order->uuid], 'en'),
        ];
        Notification::send($order->user, new GeneralWithTitleAsArrayNotification(null, $title, $title));
        return apiSuccess(new OrderResource($order), api('User Rated Successfully'));
    }

}
