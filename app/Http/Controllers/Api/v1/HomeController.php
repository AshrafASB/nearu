<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\v1\BankResource;
use App\Http\Resources\Api\v1\BranchHoursResource;
use App\Http\Resources\Api\v1\BranchResource;
use App\Http\Resources\Api\v1\CityResource;
use App\Http\Resources\Api\v1\ClassificationResource;
use App\Http\Resources\Api\v1\FavoriteResource;
use App\Http\Resources\Api\v1\ItemResource;
use App\Http\Resources\Api\v1\NationalityResource;
use App\Http\Resources\Api\v1\RateResource;
use App\Http\Resources\Api\v1\SlidersResource;
use App\Http\Resources\Api\v1\TransporterResource;
use App\Http\Resources\Api\v1\User\MerchantTypeResource;
use App\Models\Bank;
use App\Models\Branch;
use App\Models\BranchHour;
use App\Models\City;
use App\Models\Classification;
use App\Models\ClientOrderRate;
use App\Models\ContactUs;
use App\Models\Delivery;
use App\Models\Favorite;
use App\Models\Item;
use App\Models\MerchantType;
use App\Models\Nationality;
use App\Models\Order;
use App\Models\SliderImages;
use App\Models\TransporterType;
use App\Models\User;
use App\Rules\EmailRule;
use App\Rules\IntroMobile;
use App\Rules\StartWith;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class HomeController extends Controller
{

    public function termsConditions()
    {
        return apiSuccess([
            'ios_url' => setting('ios_url'),
            'about_us' => optional(setting('about_us'))[lang()],//isset($about_us) && is_array($about_us) && array_key_exists(lang(),$about_us)? $about_us[lang()]:'',
            'services' => optional(setting('services'))[lang()],// isset($services) && is_array($services) && array_key_exists(lang(),$services)? $services[lang()]:'',
            'mechanism' => optional(setting('mechanism'))[lang()],//isset($mechanism) && is_array($mechanism) && array_key_exists(lang(),$mechanism)? $mechanism[lang()]:'',
            'conditions' => optional(setting('conditions'))[lang()],// isset($conditions) && is_array($conditions) && array_key_exists(lang(),$conditions)? $conditions[lang()]:'',
        ]);
    }

    public function contactUs(Request $request)
    {
        $request->validate([
            'name' => 'required|max:250',
            'email' => ['required', 'email', 'max:50', new EmailRule()],
            'mobile' => ['required', 'min:13', 'max:13', new StartWith('+966'), new IntroMobile()],
            'message' => 'required',
        ]);

        $data = $request->only(['name', 'email', 'mobile', 'message']);
        $data['source'] = 'Mobile App';
        $contact = ContactUs::create($data);
//        TODO send notification to manager
//        Notification::send(Manager::query()->get(), new ContactUsNotification($contact));
        return apiSuccess(null, api('Message Sent Successfully'));
    }

    public function settings()
    {
        return apiSuccess([
            'order_statuses' => [
                'PENDING' => Order::PENDING,
                'ACCEPTED' => Order::ACCEPTED,
                'ON_PROGRESS' => Order::ON_PROGRESS,
                'READY' => Order::READY,
                'ON_WAY' => Order::ON_WAY,
                'COMPLETED' => Order::COMPLETED,
                'CANCELED' => Order::CANCELED,
            ],
            'delivery_statuses' => [
                'NEW_DELIVERY' => Delivery::NEW_DELIVERY,
                'ACCEPTED' => Delivery::ACCEPTED,
                'ON_WAY' => Delivery::ON_WAY,
                'COMPLETED' => Delivery::COMPLETED,
                'CANCELED' => Delivery::CANCELED,
//                'UN_KNOWN_STATUS' => Delivery::UN_KNOWN_STATUS,
            ],
            'notifications_statuses' => [
                'ACCEPTED_ORDER_NOTIFICATION' => ACCEPTED_ORDER_NOTIFICATION,
                'ON_PROGRESS_ORDER_NOTIFICATION' => ON_PROGRESS_ORDER_NOTIFICATION,
                'READY_ORDER_NOTIFICATION' => READY_ORDER_NOTIFICATION,
                'ON_WAY_ORDER_NOTIFICATION' => ON_WAY_ORDER_NOTIFICATION,
                'driver_notifications_statuses' => [
//                    'QUICK_ORDER_OFFER_NOTIFICATION' => QUICK_ORDER_OFFER_NOTIFICATION,
                    'Driver_ACCEPTED_ORDER_NOTIFICATION' => Driver_ACCEPTED_ORDER_NOTIFICATION,
                    'DRIVER_COMPLETED_ORDER_NOTIFICATION' => DRIVER_COMPLETED_ORDER_NOTIFICATION,
                    'DRIVER_CANCELED_ORDER_NOTIFICATION' => DRIVER_CANCELED_ORDER_NOTIFICATION,
                ],
                'wallet_changring_notifications_statuses' => [
                    'ADMIN_CHARGING_WALLET_NOTIFICATION' => ADMIN_CHARGING_WALLET_NOTIFICATION,
                    'ADMIN_TO_USER_NOTIFICATION' => ADMIN_TO_USER_NOTIFICATION,
                ],
            ],
            'user_types' => [
                'CLIENT' => User::CLIENT,
                'DRIVER' => User::DRIVER,
            ],


            'merchants_range' => optional(getSettings('merchants_range'))->value,
            'tax' => optional(getSettings('tax'))->value,
            'commission' => optional(getSettings('commission'))->value,
            'commission_delivery' => optional(getSettings('commission_delivery'))->value,
            'commission_cancel_delivery' => optional(getSettings('commission_cancel_delivery'))->value,
            'kilo_cost' => optional(getSettings('kilo_cost'))->value,
            'name' => optional(unserialize(optional(getSettings('name'))->value))[lang()],
            'email' => optional(getSettings('email'))->value,
            'mobile' => optional(getSettings('mobile'))->value,
            'snapchat' => optional(getSettings('snapchat'))->value,
            'logo' => optional(getSettings('logo'))->value,
            'logo_min' => optional(getSettings('logo_min'))->value,
            'whatsApp' => optional(getSettings('whatsApp'))->value,
            'facebook' => optional(getSettings('facebook'))->value,
            'twitter' => optional(getSettings('twitter'))->value,
            'instagram' => optional(getSettings('instagram'))->value,
            'youtube' => optional(getSettings('youtube'))->value,
            'android_url' => optional(getSettings('android_url'))->value,
            'ios_url' => optional(getSettings('ios_url'))->value,
            'about_us' => optional(unserialize(optional(getSettings('about_us'))->value))[lang()],
            'services' => optional(unserialize(optional(getSettings('services'))->value))[lang()],
            'mechanism' => optional(unserialize(optional(getSettings('mechanism'))->value))[lang()],
            'conditions' => optional(unserialize(optional(getSettings('conditions'))->value))[lang()],
        ]);
    }

    public function home(Request $request)
    {
        $request['except_arr_resource'] = ['items'];
        $categories = MerchantType::whereHas('merchants')->get();
        $sliders = SliderImages::query()->get();
        return apiSuccess([
            'merchant_types' => MerchantTypeResource::collection($categories),
            'slides' => SlidersResource::collection($sliders),
        ]);
    }

    public function cities()
    {
        $cities = City::query()->get();
        return apiSuccess(CityResource::collection($cities));
    }

    public function nationalities()
    {
        $nationalities = Nationality::query()->get();
        return apiSuccess(NationalityResource::collection($nationalities));
    }

    public function transporters()
    {
        $transporters = TransporterType::query()->get();
        return apiSuccess(TransporterResource::collection($transporters));
    }

    public function banks()
    {
        $banks = Bank::query()->get();
        return apiSuccess(BankResource::collection($banks));
    }

    public function branches(Request $request)
    {
        $request['except_arr_resource'] = [];
        $request->validate([
            'filter' => 'in:nearest,a-z,rating,orders,price',
            'merchant_type_id' => 'nullable|exists:merchant_types,id'
        ]);
        $lat = $request->get('lat', false);
        $lng = $request->get('lng', false);
        $merchant_type_id = $request->get('merchant_type_id', false);
        $search = $request->get('search', false);
        $filter = $request->get('filter', 'nearest');
        $merchants_range = getSettings('merchants_range');
        $merchants_range = isset($merchants_range) ? $merchants_range->value : 10;

        $branches = Branch::notMainBranch()
            ->whereHas('classifications',function ($query){
                $query->whereHas('items');
            })
            ->when($search, function ($query) use ($search) {
                $query->where('name->' . lang(), 'like', '%' . $search . '%');
            })
            ->when($merchant_type_id, function ($query) use ($merchant_type_id) {
                $query->whereHas('merchant', function ($query) use ($merchant_type_id) {
                    $query->where('merchant_type_id', $merchant_type_id);
                });
            });

        switch ($filter) {
            case 'nearest':
                $branches = $branches->when($lat && $lng, function ($query) use ($lat, $lng, $merchants_range) {
                    $query
                        ->selectRaw("branches.*,ROUND(6371 * acos( cos( radians({$lat}) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians({$lng}) ) + sin( radians({$lat}) ) * sin(radians(lat)) ) ) AS distance")
                        ->having("distance", "<", $merchants_range)
                        ->orderBy('distance', "ASC");
                });

                break;
            case 'rating':
                $branches = $branches->orderBy('rate', 'DESC');
                break;
            case 'a-z':
                $branches = $branches->orderBy('name', 'asc');
                break;
            case 'price':
                $branches = $branches->orderBy('min_price');
                break;
            case 'orders':
                $branches = $branches->withCount(['orders'])->orderBy('orders_count', 'DESC');
                break;
        }
//        dd($branches->dd());
//        if ($map == 1) {
        $branches = $branches->paginate($this->perPage);
//        } else {
//            $branches = $branches->groupBy('owner_id')->paginate($this->perPage);
//        }
        return apiSuccess([
            'items' => BranchResource::collection($branches->items()),
            'paginate' => paginate($branches),
        ]);
    }

    public function branch(Request $request, $id)
    {
        $request['except_arr_resource'] = ['classification', 'merchant'];
        $branch = Branch::query()->active()->findOrFail($id);
        $search = $request->get('search', false);
        $classifications = Classification::query()
            ->where('branch_id', $branch->id)
            ->whereHas('items', function ($query) use ($branch, $search) {
            $query->whereHas('branch', function ($query) use ($branch, $search) {
                $query->where('items.branch_id', $branch->id);
            })
                ->when($search, function ($query) use ($search) {
                $query->where('name->' . lang(), 'like', '%' . $search . '%');
            });
        })
            ->get();
        return apiSuccess([
            'branch' => new BranchResource($branch),
            'classifications' => ClassificationResource::collection($classifications),
        ]);
    }

    public function branch_meals(Request $request, $id)
    {
        $request['except_arr_resource'] = ['items', 'merchant'];

        $branch = Branch::query()->active()->findOrFail($id);
        $search = $request->get('search', false);
        $items = Item::query()->where('branch_id',$branch->id)
            ->when($search, function ($query) use ($search) {
            $query->where('name->' . lang(), 'like', '%' . $search . '%');
        })->paginate($this->perPage);
        return apiSuccess([
            'items' => ItemResource::collection($items),
            'paginate' => paginate($items),
        ]);
    }

    public function meal(Request $request, $id)
    {
        $request['except_arr_resource'] = [/*'provider',*/
            'items'];
        $item = Item::query()->findOrFail($id);
        return apiSuccess(new ItemResource($item));
    }

    public function branchInfo($id, $key)
    {
        $branch = Branch::query()->findOrFail($id);
        $res = [];
        switch ($key) {
            case 'info':
                $branches = Branch::query()->findOrFail($id);
                $work_hours = BranchHour::query()->where('branch_id', $branch->id)->orderBy('day')->get();
                $res = [
                    'branch' => new BranchResource($branch),
                    'work_hours' => BranchHoursResource::collection($work_hours),
                ];
                break;
            case 'ratings':
                $ratings = ClientOrderRate::query()->whereHas('order', function ($query) use ($branch) {
                    $query->withoutGlobalScope('paidOrder')->where('branch_id', $branch->id);
                })->latest()->get();
                $res = [
                    'branch' => new BranchResource($branch),
                    'ratings' => RateResource::collection($ratings),
                ];
                break;
            default:
                $res = [
                    'branch' => new BranchResource($branch),
                ];
        }
        $res['branches'] = BranchResource::collection(Branch::where('id','<>', $branch->id)->where('merchant_id', $branch->merchant_id)->get());
        return apiSuccess($res);
    }

    public function favorite($id)
    {
        $branch = Branch::query()->findOrFail($id);
        $user = apiUser();
        if ($branch->favorited) {
            Favorite::query()->where('user_id', $user->id)->where('branch_id', $branch->id)->delete();
        } else {
            Favorite::query()->create([
                'user_id' => $user->id,
                'branch_id' => $branch->id,
            ]);
        }
        return apiSuccess(new BranchResource($branch));
    }

    public function favorites()
    {
        $user = apiUser();
        $branches = Favorite::query()->where('user_id', $user->id)->whereHas('branch')->latest()->paginate($this->perPage);
        return apiSuccess([
            'items' => FavoriteResource::collection($branches),
            'paginate' => paginate($branches),
        ]);
    }


    public function getHours(Request $request)
    {
        $today = Carbon::now();
        $request->validate([
            'date' => 'required|date_format:Y-m-d',
            'branch_id' => 'nullable|exists:branches,id',
        ]);

        Log::emergency($request->all());
        $day = Carbon::parse($request->get('date'))->format('l');
        $am_hours = [];
        $pm_hours = [];
        $do = false;

        if (isset($request->branch_id)) {
            $work_hour = BranchHour::query()
                ->where('branch_id', $request->get('branch_id'))
                ->where('day', ex_days($day))->first();
            if ($work_hour) {
                $s_time = strtotime($work_hour->from);
                $e_time = strtotime($work_hour->to);
                $do = true;
            }
        } else {
            $s_time = strtotime(Carbon::parse('08:00:00'));
            $e_time = strtotime(Carbon::parse('13:00:00'));
            $do = true;
        }
        if ($do) {
            $startTime = date("H:i A", strtotime('+0 minutes', $s_time));
            $endTime = date("H:i A", $e_time);
            $count = 0;
            $min = 0;
            while ($startTime < $endTime && $startTime != "00:00 AM") {
                if ($endTime >= $startTime) {
                    if (Str::contains($startTime, 'PM')) {
                        $pm_hours[] = $startTime;
                    } else {
                        $am_hours[] = $startTime;
                    }
                }
                $min += 60;
                $startTime = date("H:i A", strtotime('+' . $min . ' minutes', $s_time));
                $count++;

            }
        }
        $final_am_hours = [];
        foreach ($am_hours as $am_hour) {
            $final_am_hours [] = str_replace(' AM', '', $am_hour);
        }
        $final_pm_hours = [];
        foreach ($pm_hours as $pm_hour) {
            $final_pm_hours [] = str_replace(' PM', '', $pm_hour);
        }

        return $this->sendResponse([
            'am_hours' => $final_am_hours,
            'pm_hours' => $final_pm_hours,
        ]);
    }
}
