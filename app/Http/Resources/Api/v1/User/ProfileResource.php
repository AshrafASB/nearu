<?php

namespace App\Http\Resources\Api\v1\User;

use App\Http\Resources\Api\v1\BranchResource;
use App\Http\Resources\Api\v1\CityResource;
use App\Http\Resources\Api\v1\TransporterResource;
use App\Models\ClientDriverRate;
use App\Models\DriverClientRate;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    public function toArray($request)
    {


        $except_arr_resource = $request['except_arr_resource'];
        $res = [];
        if ($this->type == User::CLIENT) {
            $res = [
                'avg_rate' => number_format($this->rate, 1),
                'total_rantings_number' => DriverClientRate::where('user_id', $this->id)->count(),
                'ranting_numbers' => [
                    "first" => DriverClientRate::where('user_id', $this->id)->where('stars_number', 1)->count(),
                    'second' => DriverClientRate::where('user_id', $this->id)->where('stars_number', 2)->count(),
                    'third' => DriverClientRate::where('user_id', $this->id)->where('stars_number', 3)->count(),
                    'fourth' => DriverClientRate::where('user_id', $this->id)->where('stars_number', 4)->count(),
                    'fifth' => DriverClientRate::where('user_id', $this->id)->where('stars_number', 5)->count(),
                ],
            ];
        } else {
            $res = [
                'avg_rate' => number_format($this->rate, 1),
                'total_rantings_number' => ClientDriverRate::where('driver_id', $this->id)->count(),
                'ranting_numbers' => [
                    "first" => ClientDriverRate::where('driver_id', $this->id)->where('stars_number', 1)->count(),
                    'second' => ClientDriverRate::where('driver_id', $this->id)->where('stars_number', 2)->count(),
                    'third' => ClientDriverRate::where('driver_id', $this->id)->where('stars_number', 3)->count(),
                    'fourth' => ClientDriverRate::where('driver_id', $this->id)->where('stars_number', 4)->count(),
                    'fifth' => ClientDriverRate::where('driver_id', $this->id)->where('stars_number', 5)->count(),
                ],
            ];
        }

        $response = [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'phone' => $this->phone,
            'email' => $this->email,
            'type' => $this->type,
            'type_name' => $this->account_type_name,
            'verified' => (bool)$this->verified,
            'status' => $this->status,
            'status_name' => $this->status_name,
            'gender' => $this->gender,
            'gender_name' => gender($this->gender),
            'fcm_token' => $this->fcm_token,
            'code' => $this->generatedCode,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'city' => new CityResource($this->city),

            'dob' => $this->dob,
            'rate' => $res,
            'local' => $this->local,
            'notification' => (bool)$this->notification,
            'unread_notifications' => (int)$this->unread_notifications,
            'wallet_balance' => $this->user_wallet,
            'access_token' => $this->access_token,

        ];

        if ($this->type == User::DRIVER) {
            $response['whatsapp'] = $this->whatsapp;
            if (!is_array($except_arr_resource) || !in_array('transporter', $except_arr_resource)) {
                $response['transporter'] = new TransporterResource($this->transporter);
            }
//            if (!is_array($except_arr_resource) || !in_array('branch', $except_arr_resource)) {
//                if ($this->driver_type == User::FOLLOWED_TO_RESTAURANT_DRIVER) $response['branch'] = new BranchResource($this->branch);
//            }
        }
        return $response;
    }
}
