<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{

    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->longText('address');
//            $table->unsignedBigInteger('address_id')->nullable();

            $table->unsignedBigInteger('branch_id');

            $table->integer('status')->default(\App\Models\Order::PENDING);
            $table->string('status_time_line')->nullable();

            $table->integer('type')->default(\App\Models\Order::DIRECT);//schedule or direct

            $table->float('total_cost');//Amount to be paid (meals_cost + delivery_cost + tax_cost + commission_cost - coupon_discount)
            $table->float('meals_cost');//The cost of the products
            $table->float('delivery_cost');
            $table->float('commission_delivery_cost');
            $table->float('driver_slice');
            $table->float('merchant_slice')->default(0);
            $table->double('distance')->default();
            $table->float('tax_cost');
            $table->float('commission_cost');
            $table->float('coupon_discount')->default(0);

            $table->unsignedBigInteger('coupon_id')->nullable();
            $table->unsignedBigInteger('payment_id')->nullable();


            $table->boolean('paid')->default(0);
            $table->integer('paid_type');
            $table->text('note')->nullable();
            $table->dateTime('pick_up_time');

            $table->integer('user_cancel')->nullable();

            $table->timestamps();
            $table->softDeletes();


            $table->tinyInteger('ordered')->default(1);
            $table->boolean('draft')->default(0);


            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('branch_id')->references('id')->on('branches')->cascadeOnDelete();
            $table->foreign('coupon_id')->references('id')->on('coupons')->cascadeOnDelete();
            $table->foreign('payment_id')->references('id')->on('payments')->cascadeOnDelete();
//            $table->foreign('address_id')->references('id')->on('addresses')->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
