<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSliderImagesTable extends Migration
{

    public function up()
    {
        Schema::create('slider_images', function (Blueprint $table) {
            $table->id();

            $table->string('image')->nullable();
            $table->integer('type')->default(1);

            $table->unsignedBigInteger('merchant_id')->nullable();
            $table->unsignedBigInteger('item_id')->nullable();
            $table->text('url')->nullable();


            $table->softDeletes();
            $table->timestamps();


            $table->tinyInteger('ordered')->default(1);
            $table->boolean('draft')->default(0);


            $table->foreign('merchant_id')->references('id')->on('merchants')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('slider_images');
    }
}
