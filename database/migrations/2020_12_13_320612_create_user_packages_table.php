<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPackagesTable extends Migration
{

    public function up()
    {
        Schema::create('user_packages', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('package_id')->nullable();
            $table->date('expire_date')->nullable();
            $table->boolean('expired')->default(false);

            $table->timestamps();
            $table->softDeletes();


            $table->tinyInteger('ordered')->default(1);
            $table->boolean('draft')->default(0);





            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade');


        });
    }

    public function down()
    {
        Schema::dropIfExists('user_packages');
    }
}
