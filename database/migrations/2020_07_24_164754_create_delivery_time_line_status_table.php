<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryTimeLineStatusTable extends Migration
{

    public function up()
    {
        Schema::create('delivery_time_line_status', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('delivery_id');
            $table->integer('key')->nullable();
            $table->string('key_name')->nullable();
            $table->timestamp('value')->nullable();
            $table->string('details')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_time_line_status');
    }
}
