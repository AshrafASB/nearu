<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->float('amount');//discount amount
            $table->enum('type', [1,2])->comment('1 ratio | 2 amount');
            $table->integer('number_users')->nullable();
            $table->integer('number_usage')->default(1);
            $table->dateTime('expire_at')->nullable();

            $table->unsignedBigInteger('merchant_id');
            $table->foreign('merchant_id')->references('id')->on('merchants')->cascadeOnDelete();



            $table->tinyInteger('ordered')->default(1);
            $table->boolean('draft')->default(0);






            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
