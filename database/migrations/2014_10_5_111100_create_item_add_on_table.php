<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemAddOnTable extends Migration
{

    public function up()
    {
        Schema::create('item_addons', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('item_id');
            $table->string('name');
            $table->float('price');
            $table->float('calories');
            $table->tinyInteger('ordered')->default(1);
            $table->boolean('draft')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('item_id')->references('id')->on('items')->cascadeOnDelete();

        });
    }

    public function down()
    {
        Schema::dropIfExists('item_addons');
    }
}
