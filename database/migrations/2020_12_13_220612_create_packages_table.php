<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{

    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('image')->nullable();
            $table->integer('months')->default(0);
            $table->double('price')->default(0);
            $table->integer('delivery_number')->default(0);
            $table->double('km_limit')->default(0);


            $table->tinyInteger('ordered')->default(1);
            $table->boolean('draft')->default(0);




            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
