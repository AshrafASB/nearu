<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{

    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('name');
            $table->string('lat');
            $table->string('lng');
            $table->string('address');
            $table->integer('type')->default(\App\Models\Address::OTHER);
//            $table->enum('type', ['home', 'work', 'other'])->default('other');
            $table->boolean('default')->default(NO);
            $table->timestamps();
            $table->softDeletes();


            $table->tinyInteger('ordered')->default(1);
            $table->boolean('draft')->default(0);




            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
