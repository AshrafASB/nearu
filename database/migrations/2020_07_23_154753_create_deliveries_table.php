<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('driver_id');
            $table->unsignedBigInteger('order_id');

            $table->integer('status')->default(\App\Models\Delivery::NEW_DELIVERY);
            $table->string('status_time_line')->nullable();

            $table->double('distance')->nullable();
            $table->time('counter')->nullable();

            $table->text('note')->nullable();

            $table->timestamps();
            $table->softDeletes();


            $table->tinyInteger('ordered')->default(1);
            $table->boolean('draft')->default(0);




            $table->foreign('driver_id')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('order_id')->references('id')->on('orders')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
