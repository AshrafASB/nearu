<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverClientRatingsTable extends Migration
{

    public function up()
    {
        Schema::create('driver_rate_client', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('driver_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('delivery_id')->nullable();
            $table->unsignedBigInteger('order_id')->nullable();

            $table->integer('stars_number')->default(0);
            $table->text('content_rating')->nullable();

            $table->timestamps();


            $table->tinyInteger('ordered')->default(1);
            $table->boolean('draft')->default(0);


            $table->foreign('driver_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('delivery_id')->references('id')->on('deliveries')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('driver_client_ratings');
    }
}
