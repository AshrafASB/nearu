<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddressIdColToOrdersTable extends Migration
{

    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('address_id')->nullable()->after('lng');
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');

        });
    }


    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('address_id');
        });
    }
}
