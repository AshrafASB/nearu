<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateItemPricesTable extends Migration
{
    public function up()
    {
        Schema::create('item_prices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('item_id');
            $table->unsignedBigInteger('option_category_id')->nullable();
            $table->float('price');
            $table->float('calories')->default(0);
            $table->boolean('isDefault')->default(0);

            $table->string('name')->nullable();
            $table->tinyInteger('ordered')->default(1);
            $table->boolean('draft')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('item_id')->references('id')->on('items')->cascadeOnDelete();
                $table->foreign('option_category_id')->references('id')->on('option_categories')->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('item_prices');
    }
}
