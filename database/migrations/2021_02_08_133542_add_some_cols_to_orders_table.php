<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeColsToOrdersTable extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->double('manager_commission_cost')->default(0);
            $table->double('tap_payment_gateway_cost')->default(0);
            $table->double('branch_slice')->default(0);
        });
    }


    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('manager_commission_cost');
            $table->dropColumn('tap_payment_gateway_cost');
            $table->dropColumn('branch_slice');
        });
    }
}
