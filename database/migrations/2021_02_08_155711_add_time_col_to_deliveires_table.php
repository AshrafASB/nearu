<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimeColToDeliveiresTable extends Migration
{
    public function up()
    {
        DB::statement('ALTER TABLE deliveries MODIFY counter  integer;');
//            DB::statement("ALTER TABLE deliveries CHANGE counter  integer;");
        Schema::table('deliveries', function (Blueprint $table) {
//            $table->integer('counter')->change();
            $table->time('time')->nullable();
        });
    }


    public function down()
    {
        Schema::table('deliveries', function (Blueprint $table) {
            DB::statement('ALTER TABLE deliveries MODIFY counter  time;');
            $table->dropColumn('time');
        });
    }
}
