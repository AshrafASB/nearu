<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientDriverRatingsTable extends Migration
{

    public function up()
    {
        Schema::create('client_rate_driver', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('driver_id')->nullable();
            $table->unsignedBigInteger('delivery_id')->nullable();
            $table->unsignedBigInteger('order_id')->nullable();
            $table->integer('stars_number')->default(0);
            $table->text('content_rating')->nullable();
            $table->timestamps();

            $table->tinyInteger('ordered')->default(1);
            $table->boolean('draft')->default(0);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('delivery_id')->references('id')->on('deliveries')->onDelete('cascade');

        });
    }

    public function down()
    {
        Schema::dropIfExists('client_driver_ratings');
    }
}
