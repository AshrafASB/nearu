<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('email')->nullable();//->unique();
            $table->string('phone');
            $table->string('whatsapp')->nullable();
            $table->boolean('notification')->default(false);
            $table->integer('status')->default(\App\Models\User::NOT_ACTIVE);
            $table->integer('type')->default(\App\Models\User::NOT_DETECTED);
            $table->integer('gender')->default(MALE);
            $table->integer('source')->default(\App\Models\User::ANDROID);
            $table->boolean('verified')->default(false);

            $table->float('rate', 2, 1)->default(0);

            $table->string('generatedCode')->nullable();

            $table->float('lat', 8, 5)->nullable();
            $table->float('lng', 8, 5)->nullable();



            $table->string('image')->nullable();
            $table->string('id_card')->nullable();//ID card
            $table->string('driving_license')->nullable();//driving license
            $table->unsignedBigInteger('city_id')->nullable();
            $table->unsignedBigInteger('nationality_id')->nullable();
            $table->unsignedBigInteger('transporter_type_id')->nullable();
            $table->unsignedBigInteger('merchant_id')->nullable();
            $table->unsignedBigInteger('branch_id')->nullable();
            $table->unsignedBigInteger('bank_id')->nullable();
            $table->string('i_ban')->nullable();
            $table->integer('driver_type')->nullable();

            $table->string('fcm_token')->nullable();//for notifications

            $table->enum('local', ['en','ar'])->default('ar');
            $table->date('dob')->nullable();

            $table->string('password')->nullable();

            $table->tinyInteger('ordered')->default(1);
            $table->boolean('draft')->default(0);

            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();

//            $table->unique(['email', 'deleted_at']);
            $table->unique(['phone', 'deleted_at']);

            $table->foreign('nationality_id')->references('id')->on('nationality')->onDelete('cascade');
            $table->foreign('transporter_type_id')->references('id')->on('transporter_type')->onDelete('cascade');
            $table->foreign('merchant_id')->references('id')->on('merchants')->onDelete('cascade');
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->foreign('bank_id')->references('id')->on('banks')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
