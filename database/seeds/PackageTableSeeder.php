<?php

use Illuminate\Database\Seeder;

class PackageTableSeeder extends Seeder
{

    public function run()
    {
        $packageIds = [];

        for ($i = 1; $i <= 3; $i++) {
            $store = \App\Models\Package::create([
                'name' => [
                    'ar' => 'package' . $i,
                    'en' => 'package' . $i,
                ],
                'months' => $i,
                'price' => $i + 50,
                'delivery_number' => $i + 20,
                'km_limit' => $i * 20,
            ]);
            $packageIds[$store->id] = [
                'expire_date' => \Carbon\Carbon::now(),
            ];
        }
        foreach (\App\Models\User::client()->get() as $index => $item) {
//            dd($packageIds);
            $item->packages()->sync($packageIds);

        }
    }
}
