<?php

use Illuminate\Database\Seeder;
use \App\Models\Classification;

class ClissifcationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Models\Merchant::get() as $index => $merchant) {
            Classification::create([
                'name' => [
                    'ar' => 'الحلويات',
                    'en' => 'Sweets',
                ],
                'merchant_id' => $merchant->id,

            ]);
            Classification::create([
                'name' => [
                    'ar' => 'الوجبات',
                    'en' => 'Meals',
                ],
                'merchant_id' => $merchant->id,

            ]);
            Classification::create([
                'name' => [
                    'ar' => 'اللحوم',
                    'en' => 'Meat',
                ],
                'merchant_id' => $merchant->id,
            ]);
            Classification::create([
                'name' => [
                    'ar' => 'المقبلات',
                    'en' => 'Appetizers',
                ],
                'merchant_id' => $merchant->id,
            ]);
            foreach ($merchant->branches as $i => $branch) {
                $classifcationIds = $merchant->classifications->pluck('id')->toArray();
                $branch->classifications()->sync($classifcationIds);
            }
        }
    }
}
