<?php

use Illuminate\Database\Seeder;

class NationalityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i <= 3; $i++){
            \App\Models\Nationality::create([
                'name' => [
                    'ar' => 'Nationality' . $i,
                    'en' => 'Nationality' . $i,
                ]

            ]);
        }
    }
}
