<?php

use Illuminate\Database\Seeder;

class TransporterTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i <= 3; $i++){
            \App\Models\TransporterType::create([
                'name' => [
                    'ar' => 'TransporterType' . $i,
                    'en' => 'TransporterType' . $i,
                ]

            ]);
        }
    }
}
