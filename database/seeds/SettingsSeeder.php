<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    public function run()
    {
        setting(['merchants_range' => 5667])->save();
        setting(['tax' => 0.01])->save();
        setting(['commission' => 0.05])->save();
        setting(['commission_delivery' => 0.05])->save();
        setting(['commission_cancel_delivery' => 7.00])->save();
        setting(['kilo_cost' => 2])->save();
        setting(['name' => [
            'ar' => 'Aloo App ar',
            'en' => 'Aloo App en',
        ]])->save();
        setting(['address' => [
            'ar' => 'address ar',
            'en' => 'address en',
        ]])->save();
        setting(['email' => 'email@email.com'])->save();
        setting(['mobile' => '+966547896541'])->save();
        setting(['logo' => ''])->save();
        setting(['logo_min' => ''])->save();
        setting(['whatsApp' => '+966547896541'])->save();
        setting(['facebook' => ''])->save();
        setting(['twitter' => ''])->save();
        setting(['instagram' => ''])->save();
        setting(['snapchat' => ''])->save();
        setting(['youtube' => ''])->save();
        setting(['android_url' => ''])->save();
        setting(['ios_url' => ''])->save();
        setting(['about_us' => [
            'ar' => 'test_ar',
            'en' => 'test_en',
        ]])->save();


        setting(['services' => [
            'ar' => 'test_ar',
            'en' => 'test_en',
        ]])->save();

        setting(['mechanism' => [
//            'ar' => 'test_ar',
            'en' => 'test_en',
        ]])->save();

        setting(['conditions' => [
            'ar' => 'test_ar',
            'en' => 'test_en',
        ]])->save();

    }
}
