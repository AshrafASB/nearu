<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class DriverTableSeeder extends Seeder
{
    public function run()
    {
        $driver = User::create([
            'name' => [
                'ar' => 'def-driver',
                'en' => 'def-driver',
            ],
            'type' => User::DRIVER,
            'driver_type' => User::FREELANCER_DRIVER,
            'status' => User::ACTIVE,
            'verified' => true,

            'lat' => 31.3547,
            'lng' => 34.3088,
            'dob' => \Carbon\Carbon::now(),


            'phone' => PHONE_DRIVER1,
            'password' => \Illuminate\Support\Facades\Hash::make(PASSWORD),
        ]);
        foreach (\App\Models\Branch::get() as $index => $branch) {
            for ($item = 1; $item <= 3; $item++) {
                $driver = User::create([
                    'name' => [
                        'ar' => 'driver' . $item,
                        'en' => 'driver' . $item,
                    ],
                    'verified' => true,
                    'driver_type' => User::FOLLOWED_TO_RESTAURANT_DRIVER,
                    'merchant_id' => $branch->merchant_id,
                    'branch_id' => $branch->id,
                    'type' => User::DRIVER,
                    'phone' => '+9665' . getRandomPhoneNumber_8_digit(),
                    'password' => \Illuminate\Support\Facades\Hash::make(PASSWORD),
                ]);

            }
        }
    }
}
