<?php

use Illuminate\Database\Seeder;
use  \App\Models\Merchant;
use  \App\Models\Item;

class MerchantSeeder extends Seeder
{
    public function run()
    {
        $optionSizeCategory = \App\Models\OptionCategory::create([
            'name' => [
                'ar' => 'الحجم',
                'en' => 'الحجم',
            ],
        ]);
        $merchant1 = Merchant::create([
            'name' => [
                'ar' => 'def-Merchant',
                'en' => 'def-Merchant',
            ],
//            'drivers_code' => '12345',//generateRandomString(4) . '1',
            'phone' => PHONE_MERCHANT1,
            'email' => EMAIL_MERCHANT1,
            "lng" => 31.5389666,//Al Quds Open University - Northern Gaza
            "lat" => 34.5072628,//Al Quds Open University - Northern Gaza

            'accepted' => true,


            'max_orders' => 5,
            'password' => \Illuminate\Support\Facades\Hash::make(PASSWORD),
            'status' => Merchant::ACTIVE,
            'city_id' => \App\Models\City::inRandomOrder()->first()->id,
            'Merchant_type_id' => \App\Models\MerchantType::inRandomOrder()->first()->id,

        ]);
        $this->createMercahntBranches($merchant1, 3);
        $this->createOtherMerchants(4);


//        $this->createMercahntBranchesWithClassifcaitonsWithHrsWithItemsWithCoupons($merchant1);
//        $this->createMercahntClassifications($merchant1);
//        $this->createMercahntItems($merchant1);
        //        create createMercahntCoupons
//        $this->createMercahntCoupons($merchant1);
//        dd('done');
    }

    private function createMercahntBranches($merchant1, $counter = 1)
    {
        for ($i = 1; $i <= $counter; $i++) {
            $branch1 = \App\Models\Branch::create([
                'uuid' => date('Y') . date('m') . $counter,
                'name' => [
                    'ar' => 'branch - ' . ($merchant1->id + $i),
                    'en' => 'branch - ' . ($merchant1->id + $i),
                ],
                'merchant_id' => $merchant1->id,
                'phone' => '+96600577700' . $counter,
                'email' => 'branch' . ($merchant1->id + $i) . '@gmail.com',
                "lng" => 31.5389666,//Al Quds Open University - Northern Gaza
                "lat" => 34.5072628,//Al Quds Open University - Northern Gaza
                'max_orders' => 5,
                'drivers_code' => generateRandomString(3) . '1' . ($counter + 1),
                'isMainBranch' => \App\Models\Branch::mainBranch()->where('merchant_id', $merchant1->id)->count() == 0 ? true : false,
                'password' => \Illuminate\Support\Facades\Hash::make(PASSWORD),
                'status' => Merchant::ACTIVE,
                'accepted' => true,
                'city_id' => \App\Models\City::inRandomOrder()->first()->id,
            ]);
            $this->createBranchClassifications($merchant1, $branch1, 4);

            $this->createBranchHourse($branch1, 7);
//            //        link those Coupons with branches
            $couponsIds = $merchant1->coupons->pluck('id')->toArray();
            $branch1->coupons()->sync($couponsIds);
        }
    }

    private function createBranchClassifications($merchant1, $branch1, $counter = 1)
    {
        for ($clas = 1; $clas <= $counter; $clas++) {
            $class1 = $merchant1->classifications()->create([
                'name' => [
                    'ar' => 'Sweets-' . ($merchant1->id + $branch1->id + $clas),
                    'en' => 'Sweets-' . ($merchant1->id + $branch1->id + $clas),
                ],
                'branch_id' => $branch1->id
            ]);
            if ($clas != 1) $this->createBranchItems($merchant1, $branch1, $class1, 4);
        }


    }

    private function createBranchItems($merchant1, $branch1, $class1, $counter = 1)
    {
        for ($i = 1; $i <= $counter; $i++) {
            $item = Item::create([
                'name' => [
                    'ar' => 'Item - ' . ($merchant1->id + $branch1->id + $class1->id + $i),
                    'en' => 'Item - ' . ($merchant1->id + $branch1->id + $class1->id + $i),
                ],
                'description' => [
                    'ar' => 'description - ' . $i,
                    'en' => 'description - ' . $i,
                ],
                'calories' => $i + 5,
                'merchant_id' => $branch1->merchant_id,
                'branch_id' => $branch1->id,
                'classification_id' => $class1->id,
            ]);
            for ($j = 1; $j <= 3; $j++) {
                \App\Models\ItemPrice::create([
                    'item_id' => $item->id,
                    'isDefault' => ($j == 1) ? true : false,
                    'option_category_id' => \App\Models\OptionCategory::first()->id,
                    'price' => $j + 7,
                    'calories' => $j + 7,
                    'name' => [
                        'ar' => 'Item price - ' . $j,
                        'en' => 'Item prive - ' . $j,
                    ],
                ]);
            }
            for ($c = 1; $c <= 3; $c++) {
                \App\Models\ItemAddon::create([
                    'item_id' => $item->id,
                    'price' => $c + 7,
                    'calories' => $c + 7,
                    'name' => [
                        'ar' => 'Item addon - ' . $c,
                        'en' => 'Item addon - ' . $c,
                    ],
                ]);
            }
        }
    }

    private
    function createMercahntBranchesWithClassifcaitonsWithHrsWithItemsWithCoupons($merchant1, $counter = 0)
    {

//        $lastBranch = getLastBranchId($merchant1->id);
        $branch1 = $merchant1->branches()->create([
            'uuid' => date('Y') . date('m') . $counter,
            'name' => [
                'ar' => 'branch' . $counter,
                'en' => 'branch' . $counter,
            ],
            'phone' => '+96600577700' . $counter,
            'email' => 'branch' . $counter . '@gmail.com',
            "lng" => 31.5389666,//Al Quds Open University - Northern Gaza
            "lat" => 34.5072628,//Al Quds Open University - Northern Gaza
            'max_orders' => 5,
            'drivers_code' => generateRandomString(3) . '1' . ($counter + 1),
            'isMainBranch' => count($merchant1->branches) > 1 ? false : true,
            'password' => \Illuminate\Support\Facades\Hash::make(PASSWORD),
            'status' => Merchant::ACTIVE,
            'accepted' => true,
            'city_id' => \App\Models\City::inRandomOrder()->first()->id,
        ]);
//        link those classifications with branches
//        $classifcationIds = $merchant1->classifications->pluck('id')->toArray();
//        $branch1->classifications()->sync($classifcationIds);
        $this->createBranchHourse($branch1, 7);

        //        link those Items with branches
//        $itemsIds = $merchant1->items->pluck('id')->toArray();
//        $branch1->items()->sync($itemsIds);


        //        link those Coupons with branches
        $couponsIds = $merchant1->coupons->pluck('id')->toArray();
        $branch1->coupons()->sync($couponsIds);
    }

    private
    function createMercahntCoupons($merchant1, $counter = 0)
    {
        $merchant1->coupons()->create([
            'code' => '@#Vv20' . $counter,
            'amount' => 10,
            'type' => range(1, 2)[array_rand(range(1, 2))],
            'number_users' => range(1, 10)[array_rand(range(1, 10))],
            'number_usage' => range(10, 20)[array_rand(range(10, 20))],
            'expire_at' => \Carbon\Carbon::now()->addDays(5),
        ]);

        $merchant1->coupons()->create([
            'code' => 'Ramadan20',
            'amount' => 10,
            'type' => range(1, 2)[array_rand(range(1, 2))],
            'number_users' => range(1, 10)[array_rand(range(1, 10))],
            'number_usage' => range(10, 20)[array_rand(range(10, 20))],
            'expire_at' => \Carbon\Carbon::now()->addDays(5),
        ]);
    }

    private
    function createMercahntClassifications($merchant1)
    {
        foreach ($merchant1->branches as $index => $branch) {
            $class1 = $merchant1->classifications()->create([
                'name' => [
                    'ar' => 'الحلويات',
                    'en' => 'Sweets',
                ],
                'branch_id' => $branch->id
            ]);
            $class2 = $merchant1->classifications()->create([
                'name' => [
                    'ar' => 'الوجبات',
                    'en' => 'Meals',
                ],
                'branch_id' => $branch->id

            ]);
            $class3 = $merchant1->classifications()->create([
                'name' => [
                    'ar' => 'اللحوم',
                    'en' => 'Meat',
                ],
                'branch_id' => $branch->id

            ]);
            $class4 = $merchant1->classifications()->create([
                'name' => [
                    'ar' => 'المقبلات',
                    'en' => 'Appetizers',
                ],
                'branch_id' => $branch->id

            ]);
        }


    }

    private
    function createMercahntItems($merchant1, $itemsNumber = 5)
    {
        foreach ($merchant1->branches as $index => $branch) {
            for ($i = 1; $i <= $itemsNumber; $i++) {
                $item = Item::create([
                    'name' => [
                        'ar' => 'Item - ' . $i,
                        'en' => 'Item - ' . $i,
                    ],
                    'description' => [
                        'ar' => 'description - ' . $i,
                        'en' => 'description - ' . $i,
                    ],
                    'calories' => $i + 5,
                    'merchant_id' => $merchant1->id,
                    'branch_id' => $branch->id,
                    'classification_id' => \App\Models\Classification::currentMerchant($merchant1->id)
                        ->inRandomOrder()->first()->id,
                ]);
                for ($j = 1; $j <= 3; $j++) {
                    \App\Models\ItemPrice::create([
                        'item_id' => $item->id,
                        'isDefault' => ($j == 1) ? true : false,
                        'option_category_id' => \App\Models\OptionCategory::first()->id,
                        'price' => $j + 7,
                        'calories' => $j + 7,
                        'name' => [
                            'ar' => 'Item price - ' . $j,
                            'en' => 'Item prive - ' . $j,
                        ],
                    ]);
                }
                for ($c = 1; $c <= 3; $c++) {
                    \App\Models\ItemAddon::create([
                        'item_id' => $item->id,
                        'price' => $c + 7,
                        'calories' => $c + 7,
                        'name' => [
                            'ar' => 'Item addon - ' . $c,
                            'en' => 'Item addon - ' . $c,
                        ],
                    ]);
                }
            }
//        dd(count($merchant1->items));
//        Has discount items
            for ($i = 1; $i <= $itemsNumber; $i++) {
                $item = Item::create([
                    'name' => [
                        'ar' => 'Item - ' . ($i + 10),
                        'en' => 'Item - ' . ($i + 10),
                    ],
                    'description' => [
                        'ar' => 'description - ' . $i,
                        'en' => 'description - ' . $i,
                    ],
                    'calories' => $i + 5,
                    'has_discount' => true,
                    'discount' => $i,
                    'merchant_id' => $merchant1->id,
                    'branch_id' => $branch->id,
                    'classification_id' => \App\Models\Classification::currentMerchant($merchant1->id)
                        ->inRandomOrder()->first()->id,
                ]);
                for ($j = 1; $j <= 3; $j++) {
                    \App\Models\ItemPrice::create([
                        'item_id' => $item->id,
                        'isDefault' => ($j == 1) ? true : false,
                        'option_category_id' => \App\Models\OptionCategory::first()->id,
                        'price' => $j + 7,
                        'calories' => $j + 7,
                        'name' => [
                            'ar' => 'Item price - ' . $j,
                            'en' => 'Item prive - ' . $j,
                        ],
                    ]);
                }
                for ($c = 1; $c <= 3; $c++) {
                    \App\Models\ItemAddon::create([
                        'item_id' => $item->id,
                        'price' => $c + 7,
                        'calories' => $c + 7,
                        'name' => [
                            'ar' => 'Item addon - ' . $c,
                            'en' => 'Item addon - ' . $c,
                        ],
                    ]);
                }
            }
        }
    }


    private function createBranchHourse($branch1, $itemsNumber)
    {
        for ($i = 1; $i <= $itemsNumber; $i++) {
            $branch1->hours()->create([
                'day' => $i,
                'from' => '6:00:00',// \Carbon\Carbon::now()->format('H:i A'), // 8 am
                'to' => '23:00:00',// \Carbon\Carbon::now()->addHour(12)->format('H:i A'), // 4 pm
            ]);
        }
    }


    private function createOtherMerchants($itemsNumber)
    {
        for ($item = 1; $item <= $itemsNumber; $item++) {
            $merchant1 = Merchant::create([
                'name' => [
                    'ar' => 'def-Merchant' . $item,
                    'en' => 'def-Merchant' . $item,
                ],
                'phone' => '+966' . getRandomPhoneNumber_8_digit(),
                'email' => 'merchant' . $item . '@gmail.com',
                "lng" => 31.5389666,//Al Quds Open University - Northern Gaza
                "lat" => 34.5072628,//Al Quds Open University - Northern Gaza

                'max_orders' => 2,
                'password' => \Illuminate\Support\Facades\Hash::make(PASSWORD),
                'status' => Merchant::ACTIVE,
                'accepted' => true,
                'city_id' => \App\Models\City::inRandomOrder()->first()->id,
                'Merchant_type_id' => \App\Models\MerchantType::find($item)->id,
            ]);
            $this->createMercahntBranches($merchant1, 3);

        }
    }
}
