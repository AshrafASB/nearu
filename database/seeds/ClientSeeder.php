<?php

use Illuminate\Database\Seeder;
use \App\Models\User;
use \App\Models\Order;
use \App\Models\Branch;
use App\Models\ClientOrderRate;
use App\Models\ClientDriverRate;
use App\Models\OrderItem;
use Carbon\Carbon;
use \App\Models\Delivery;


class ClientSeeder extends Seeder
{
    public function run()
    {
        $client = User::create([
            'name' => [
                'ar' => 'def-client',
                'en' => 'def-client',
            ],
            'email' => 'def-client@client.com',
            'type' => User::CLIENT,
            'status' => User::ACTIVE,
            'phone' => PHONE_CLIENT1,
            'whatsapp' => PHONE_CLIENT1,
            'verified' => true,

            'lat' => 31.5347908, //Indonesian hospital
            'lng' => 34.5102229,//Indonesian hospital
            'dob' => \Carbon\Carbon::now(),

            'city_id' => \App\Models\City::inRandomOrder()->first()->id,

            'password' => \Illuminate\Support\Facades\Hash::make(PASSWORD),
        ]);
        \App\Models\Address::create([
            'user_id' => $client->id,
            'name' => 'name',
            'lat' => 31.5347908, //Indonesian hospital
            'lng' => 34.5102229,//Indonesian hospital
            'address' => 'test',
            'type' => \App\Models\Address::HOME,
            'default' => true,
        ]);


//        $this->order($client);
//        $this->order_ready($client);
//        $this->wallet($client);


//        $client->packages()->sync([\App\Models\Package::inRandomOrder()->first()->id]);

        for ($item = 1; $item <= 3; $item++) {
            $client = User::create([
                'name' => [
                    'ar' => 'client' . $item,
                    'en' => 'client' . $item,
                ],
                'email' => 'def-client' . $item . '@client.com',
                'dob' => \Carbon\Carbon::now(),

                'type' => User::CLIENT,
                'status' => User::ACTIVE,
                'verified' => true,

                'lat' => 31.5347908, //Indonesian hospital
                'lng' => 34.5102229,//Indonesian hospital

                'city_id' => \App\Models\City::inRandomOrder()->first()->id,

                'phone' => '+9665' . getRandomPhoneNumber_8_digit(),
                'password' => \Illuminate\Support\Facades\Hash::make(PASSWORD),
            ]);

            \App\Models\Address::create([
                'user_id' => $client->id,
                'name' => 'name',
                'lat' => 31.5347908, //Indonesian hospital
                'lng' => 34.5102229,//Indonesian hospital
                'address' => 'address',
                'type' => \App\Models\Address::OTHER,
                'default' => true,
            ]);
//            $client->packages()->sync([\App\Models\Package::inRandomOrder()->first()->id]);

        }
    }

    private function wallet($client)
    {
        \App\Models\Wallet::create([
            'user_id' => $client->id,
            't_type' => \App\Models\Wallet::NEW_ORDER,
            'amount' => 100,
            'note' => 'test',
            'order_id' => $client->orders->first()->id,
        ]);
        \App\Models\Wallet::create([
            'user_id' => $client->id,
            't_type' => \App\Models\Wallet::CANCEL_ORDER,
            'note' => 'test',
            'amount' => 100,
            'order_id' => $client->orders->first()->id,
        ]);
        \App\Models\Wallet::create([
            'user_id' => $client->id,
            't_type' => \App\Models\Wallet::ADMIN_CHARGING,
            'amount' => 150,
            'note' => 'test',
        ]);
    }

    private function order($client)
    {
        $order = Order::query()->create([
            'user_id' => $client->id,
            'uuid' => '2020212',
            'branch_id' => Branch::first()->id,
            'address' => serialize(\App\Models\Address::first()),
            'status' => Order::COMPLETED,
            'status_time_line' => json_encode([
                getAnonymousStatusObj(Order::PENDING, 'PENDING', \Carbon\Carbon::now()->format(DATE_FORMAT_FULL), api('Order Pending')),
            ]),
            'type' => 1,
            'total_cost' => 100,
            'meals_cost' => 100,
            'delivery_cost' => 100,
            'commission_delivery_cost' => 100,
            'driver_slice' => 100,
            'tax_cost' => 200,
            'commission_cost' => 100,
            'coupon_discount' => 100,
            'coupon_id' => 1,
            'paid' => 1,
            'paid_type' => 1,
            'pick_up_time' => Carbon::now()->addHour(),
            'note' => 'note',
        ]);
        $item = \App\Models\Item::inRandomOrder()->first();
        $order_item = OrderItem::create([
            'order_id' => $order->id,
            'item_id' => $item->id,
            'quantity' => 2,
            'item_price_id' => $item->prices->first()->id,
            'price' => 10,
            'amount' => 10 * 2,
        ]);
        ClientOrderRate::query()->create([
            'order_id' => $order->id,
            'branch_id' => $order->branch_id,
            'user_id' => $client->id,
            'stars_number' => 3,
            'content_rating' => 'comment',
        ]);
        $rates = ClientOrderRate::query()->whereHas('order', function ($query) use ($order) {
            $query->where('order_id', $order->id);
        })->avg('stars_number');
        $order->branch()->update([
            'rate' => (float)$rates,
        ]);
        $delivery = Delivery::create([
            'driver_id' => \App\Models\User::driver()->first()->id,
            'order_id' => $order->id,
            'status' => Delivery::COMPLETED,
            'distance' => 55,
            'counter' => \Carbon\Carbon::now(),
        ]);
        ClientDriverRate::query()->create([
            'order_id' => $order->id,
            'delivery_id' => $delivery->id,
            'driver_id' => \App\Models\User::driver()->first()->id,
            'user_id' => $client->id,
            'stars_number' => 3,
            'content_rating' => 'comment',
        ]);
        $rates = ClientDriverRate::query()
            ->whereHas('order', function ($query) use ($order) {
                $query->where('order_id', $order->id);
            })->avg('stars_number');
        $delivery->driver()->update([
            'rate' => (float)$rates,
        ]);
    }

    private function order_ready($client)
    {
        $order = Order::query()->create([
            'user_id' => $client->id,
            'uuid' => '2020212',
            'branch_id' => Branch::first()->id,
            'address' => serialize(\App\Models\Address::first()),
            'status' => Order::ON_WAY,
            'status_time_line' => json_encode([
                getAnonymousStatusObj(Order::ON_WAY, 'ON_WAY', Carbon::now()->format(DATE_FORMAT_FULL), api('Order On Way')),
            ]),

            'type' => 1,
            'total_cost' => 100,
            'meals_cost' => 100,
            'delivery_cost' => 100,
            'commission_delivery_cost' => 100,
            'driver_slice' => 100,
            'tax_cost' => 200,
            'commission_cost' => 100,
            'coupon_discount' => 100,
            'coupon_id' => 1,
            'paid' => 1,
            'paid_type' => 1,
            'pick_up_time' => Carbon::now()->addHour(),
            'note' => 'note',
        ]);
        Delivery::create([
            'driver_id' => \App\Models\User::driver()->first()->id,
            'order_id' => $order->id,
            'status' => Delivery::ON_WAY,
            'distance' => 55,
            'counter' => \Carbon\Carbon::now(),
        ]);
    }
}

