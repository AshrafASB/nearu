<?php

use Illuminate\Database\Seeder;

class SliderImagesTableSeeder extends Seeder
{

    public function run()
    {
        \App\Models\SliderImages::create([
            'type' => \App\Models\SliderImages::URL_EXTERNAL,
            'url' => 'http://lorempixel.com/640/480/',
        ]);


        \App\Models\SliderImages::create([
            'type' => \App\Models\SliderImages::MERCHANT,
            'merchant_id' => 1,
        ]);

    }
}
