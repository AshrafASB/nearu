<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        dd(\App\Models\Merchant::first()->branches()->count());
        $this->call(ManagerTableSeeder::class);

        $this->call(MerchantTypesSeeder::class);
        $this->call(NationalityTableSeeder::class);
        $this->call(TransporterTypeTableSeeder::class);
        $this->call(BankTableSeeder::class);


        $this->call(CityTableSeeder::class);


        $this->call(MerchantSeeder::class);
        $this->call(DriverTableSeeder::class);
        $this->call(ClientSeeder::class);
        $this->call(PackageTableSeeder::class);

        $this->call(SliderImagesTableSeeder::class);

        $this->call(SettingsSeeder::class);
        $this->call(PermissionTableSeeder::class);

        $this->call(ISeedOauthPersonalAccessClientsTableSeeder::class);
        $this->call(ISeedOauthClientsTableSeeder::class);

    }
}
