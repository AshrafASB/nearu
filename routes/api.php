<?php


Route::group(['prefix' => 'v1', 'namespace' => ROOT_NAMESPACE, "middleware" => ['localization']], function () {

    Route::get('terms&conditions', 'HomeController@termsConditions');
    Route::get('cities', 'HomeController@cities');
    Route::get('nationalities', 'HomeController@nationalities');
    Route::get('transporters', 'HomeController@transporters');
    Route::get('banks', 'HomeController@banks');
    Route::get('home', 'HomeController@home');
    Route::get('getAllHours/{id}', 'OrderController@getAllHours');
    Route::get('get_hours', 'HomeController@getHours');
    Route::get('branches', 'HomeController@branches');
    Route::get('branch/{id}', 'HomeController@branch');
    Route::get('branch_meals/{id}', 'HomeController@branch_meals');
    Route::get('meal/{id}', 'HomeController@meal');
    Route::get('branch/{id}/{key}', 'HomeController@branchInfo');
    Route::get('settings', 'HomeController@settings');
    Route::post('contact_us', 'HomeController@contactUs');



    Route::get('get_branch_ratings/{id}', 'RateController@get_branch_ratings');
    Route::get('get_driver_ratings/{id}', 'RateController@get_driver_ratings');
    Route::get('get_user_ratings/{id}', 'RateController@get_user_ratings');
    Route::post('client_rate_branch/{id}', 'RateController@client_rate_branch')->middleware(["auth:api", "CheckIsActive", "CheckIsVerified", 'CheckIsClient']);
    Route::post('client_rate_driver/{id}', 'RateController@client_rate_driver')->middleware(["auth:api", "CheckIsActive", "CheckIsVerified", 'CheckIsClient']);;
    Route::post('driver_rate_client/{id}', 'RateController@driver_rate_client')->middleware(["auth:api", "CheckIsActive", "CheckIsVerified", 'CheckIsDriver']);;





    Route::group(['prefix' => 'user'], function () {
        //check Coupon
        Route::post('check_coupon', 'OrderController@checkCoupon');
        Route::post('check_order', 'OrderController@checkOrder');
        Route::post('upload_file', 'HomeController@uploadFile');
    });


    Route::group(["middleware" => ["auth:api", "CheckIsActive", "CheckIsVerified"]], function () {

        Route::group(['prefix' => 'driver', 'namespace' => 'Driver', "middleware" => ["CheckIsDriver"]], function () {
            Route::get('deliveries', 'DriverController@deliveries');
            Route::post('update_location', 'DriverController@update_location');
            Route::post('rateDelivery/{id}', 'DriverController@rateDelivery');
            Route::get('delivery/{id}', 'DriverController@delivery');
            Route::post('notification_settings', 'DriverController@notification_settings');
            Route::post('changeDeliveryStatus', 'DriverController@changeDeliveryStatus');
            Route::get('rates', 'DriverController@rates');

        });

        Route::group(['prefix' => 'user',], function () {
            Route::group(["middleware" => ["CheckIsClient"]], function () {
                Route::post('favorite/{id}', 'HomeController@favorite');
                Route::get('favorites', 'HomeController@favorites');
                Route::apiResource('address', 'User\AddressController');
                Route::get('packages', 'User\PackagesController@packages');
                Route::post('buy_package', 'User\PackagesController@buy_package');

//              Orders
                Route::post('checkout', 'OrderController@checkout')/*->middleware('CheckClientPackage')*/;
                Route::get('orders/{key}', 'OrderController@orders');
                Route::get('order/{id}/currentTrackMap', 'OrderController@currentTrackMap');
                Route::get('order/{id}', 'OrderController@order');
                Route::post('rate_order/{id}', 'OrderController@rateOrder');
                Route::post('cancel_order/{id}', 'OrderController@cancelOrder');
                Route::post('confirm_order/{id}', 'OrderController@confirmOrder');
                Route::get('re_order/{id}', 'OrderController@reOrder');
            });


            Route::get('notifications', 'User\UserController@notifications');
            Route::get('wallet', 'User\UserController@wallet');
            Route::get('notification/{id}', 'User\UserController@notification');
            Route::get('profile', 'User\UserController@profile');
            Route::post('update_profile', 'User\UserController@updateProfile');
            Route::post('update_mobile', 'User\UserController@updateMobile');
            Route::post('update_language', 'User\UserController@updateLanguage');
            Route::post('update_notification', 'User\UserController@updateNotification');
            Route::post('update_image', 'User\UserController@updateImage');
        });
    });

//     All API Auth Routes here
    Route::group(['namespace' => 'Auth'], function () {
        Route::post('login', 'AuthController@login');
        Route::post('register', 'AuthController@register')->middleware('auth:api');
        Route::post('signup_delivery_build_account', 'AuthController@signup_delivery_build_account')->middleware(['api', 'CheckIsDriver']);
        Route::post('logoutAllAuthUsers', 'AuthController@logoutAllAuthUsers');
        Route::post('resendCode', 'AuthController@resendCode');
        Route::post('verified_code', 'AuthController@verified_code');
        Route::group(['middleware' => 'auth:api'], function () {
            Route::post('logout', 'AuthController@logout');
        });
    });
    Route::group(['namespace' => 'notifications', 'prefix' => 'notifications'], function () {
        Route::post('saveFcmToken', 'NotificationsController@SaveFCMToken');
        Route::post('sendNotificationForAllUsers', 'NotificationsController@sendNotificationForAllUsers');


    });
});


